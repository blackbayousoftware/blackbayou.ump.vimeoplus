﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheOutfield.UmbExt.UniversalMediaPicker.Controls;
using TheOutfield.UmbExt.UniversalMediaPicker.Extensions;

namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus
{
    public class VimeoPlusConfigControl : AbstractConfigControl
    {
        private const string TEXTBOX_CLASSES = "guiInputText guiInputStandardSize";
        
        private VimeoPlusConfig _config;
        private TextBox authKeyTextBox = new TextBox();
        private TextBox userIdTextBox = new TextBox();
        private TextBox searchQueryTextBox = new TextBox();
        private TextBox sortByTextBox = new TextBox();
        private TextBox sortDirectionTextBox = new TextBox();
        private TextBox licenseFilterTextBox = new TextBox();
        private TextBox thumbnailHeightTextBox = new TextBox();
        private TextBox pageSizeTextBox = new TextBox();

        public override string Value
        {
            get
            {
                EnsureChildControls();

                return new VimeoPlusConfig
                {
                    AuthKey = authKeyTextBox.Text,
                    UserId = userIdTextBox.Text,
                    SearchText = sortByTextBox.Text,
                    SortBy = searchQueryTextBox.Text,
                    SortDirection = sortDirectionTextBox.Text,
                    LicenseFilter = licenseFilterTextBox.Text,
                    ThumbnailHeight = int.Parse(thumbnailHeightTextBox.Text),
                    PageSize = int.Parse(thumbnailHeightTextBox.Text),
                
                }
                .SerializeToJson();
            }
        }

        public VimeoPlusConfigControl(string config)
        {
            _config = config.DeserializeJsonTo<VimeoPlusConfig>();
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                authKeyTextBox.Text = _config.AuthKey;
                userIdTextBox.Text = _config.UserId;
                searchQueryTextBox.Text = _config.SearchText;
                sortByTextBox.Text = _config.SortBy;
                sortDirectionTextBox.Text = _config.SortDirection;
                licenseFilterTextBox.Text = _config.LicenseFilter;
                thumbnailHeightTextBox.Text = _config.ThumbnailHeight.ToString();
                pageSizeTextBox.Text = _config.PageSize.ToString();
            }

            base.OnLoad(e);
        }

        protected override void CreateChildControls()
        {
            // Set the css class of the textboxes
            authKeyTextBox.CssClass = TEXTBOX_CLASSES;
            userIdTextBox.CssClass = TEXTBOX_CLASSES;
            sortByTextBox.CssClass = TEXTBOX_CLASSES;
            searchQueryTextBox.CssClass = TEXTBOX_CLASSES;
            sortDirectionTextBox.CssClass = TEXTBOX_CLASSES;
            licenseFilterTextBox.CssClass = TEXTBOX_CLASSES;
            thumbnailHeightTextBox.CssClass = TEXTBOX_CLASSES;
            pageSizeTextBox.CssClass = TEXTBOX_CLASSES;

            // Add the controls to the controls collection
            Controls.Add(authKeyTextBox);
            Controls.Add(userIdTextBox);
            Controls.Add(sortByTextBox);
            Controls.Add(sortDirectionTextBox);
            Controls.Add(searchQueryTextBox);
            Controls.Add(licenseFilterTextBox);
            Controls.Add(thumbnailHeightTextBox);
            Controls.Add(pageSizeTextBox);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.AddFormRow("Auth Key:", "Please enter your authentication key", authKeyTextBox);
            writer.AddFormRow("User Id:", "Please enter your vimeo user id", userIdTextBox);
            writer.AddFormRow("Search Query:", "Please enter the search query to append to all searches", searchQueryTextBox);
            writer.AddFormRow("Sort By:", "Please enter the default sort by option", sortByTextBox);
            writer.AddFormRow("Sort Direction", "Please enter the default sort direction", sortDirectionTextBox);
            writer.AddFormRow("License Filter", "Please enter the license filter", licenseFilterTextBox);
            writer.AddFormRow("Thumbnail Height", "Please enter the default thumbnail height", thumbnailHeightTextBox);
            writer.AddFormRow("Page Size", "Please enter the default result page size", pageSizeTextBox);
        }
    }
}