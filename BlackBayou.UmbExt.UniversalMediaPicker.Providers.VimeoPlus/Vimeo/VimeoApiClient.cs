﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Vimeo
{
    using BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model;
    using BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Vimeo.Authentication;
    using RestSharp;
    using RestSharp.Deserializers;
    using System;
    using System.Collections.Generic;

    public class VimeoApiClient
    {
        private readonly RestClient apiClient;

        public VimeoApiClient(string accessToken)
        {
            apiClient = new RestClient("https://api.vimeo.com")
            {
                Authenticator = new VimeoAuthenticator(accessToken)
            };
            apiClient.AddHandler("application/vnd.vimeo.video+json", new JsonDeserializer());
        }

        public IList<VimeoVideo> SearchVideos(VideoSearchOptions searchOptions)
        {
            if (searchOptions == null) throw new ArgumentNullException("searchOptions");
            var requestUrl = string.IsNullOrEmpty(searchOptions.UserId) ?
                "videos" :
                "users/{userId}/videos";
            var currentPage = searchOptions.Page;
            var fetchRecords = true;
            var result = new List<VimeoVideo>();

            while (fetchRecords)
            {
                var request = new RestRequest(requestUrl, Method.GET)
                {
                    RequestFormat = DataFormat.Json
                };
                if (!string.IsNullOrEmpty(searchOptions.UserId))
                {
                    request.AddUrlSegment("userId", searchOptions.UserId);
                }

                if (!string.IsNullOrEmpty(searchOptions.Text))
                {
                    request.AddParameter("query", searchOptions.Text);

                }
                request.AddParameter("page", currentPage);
                request.AddParameter("per_page", searchOptions.PageSize);

                if (!string.IsNullOrEmpty(searchOptions.SortBy))
                {
                    request.AddParameter("sort", searchOptions.SortBy);
                }

                if (!string.IsNullOrEmpty(searchOptions.SortDirection))
                {
                    request.AddParameter("direction", searchOptions.SortDirection);
                }

                if (!string.IsNullOrEmpty(searchOptions.LicenseFilter))
                {
                    request.AddParameter("filter", searchOptions.LicenseFilter);
                }

                var response = apiClient.Execute<ApiResponse<List<VimeoVideo>>>(request);
                result.AddRange(response.Data.Data);
                currentPage++;
                fetchRecords = result.Count < response.Data.Total;
            }

            return result;
        }

        public VimeoVideo GetVideo(long clipId)
        {
            var request = new RestRequest("videos/{videoId}", Method.GET);
            request.AddUrlSegment("videoId", clipId.ToString());

            var response = apiClient.Execute<VimeoVideo>(request);
            return response.Data;
        }
    }
}