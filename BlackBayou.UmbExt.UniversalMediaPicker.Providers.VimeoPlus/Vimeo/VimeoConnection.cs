﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Vimeo
{
    using BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model;
    using System.Collections.Generic;
    using System.Runtime.Caching;
    using System;

    internal sealed class VimeoConnection
    {
        internal string AuthToken { get; private set; }

        private VimeoApiClient VimeoApiClient { get; set; }

        private MemoryCache MemoryCache { get; set; }

        public VimeoConnection(string authToken)
        {
            this.AuthToken = authToken;
            this.VimeoApiClient = new VimeoApiClient(this.AuthToken);

            // scope cache to this connection only
            this.MemoryCache = new MemoryCache(this.AuthToken);
        }


        public IList<VimeoVideo> GetVideos(VideoSearchOptions searchOptions)
        {
            List<VimeoVideo> results = new List<VimeoVideo>();
            results.AddRange(VimeoApiClient.SearchVideos(searchOptions));
            results.ForEach(v => this.CacheVimeoVideo(v));
            return results;
        }

        /// <summary>
        /// Get video from cache, otherwise request then add to cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public VimeoVideo GetVideo(long clipId)
        {
            VimeoVideo video = this.MemoryCache.Get(clipId.ToString()) as VimeoVideo;

            if (video == null)
            {
                try
                {
                    video = this.VimeoApiClient.GetVideo(clipId);
                }
                catch
                {
                    video = null;
                }

                if (video != null)
                {
                    video = this.CacheVimeoVideo(video);
                }
            }

            return video;
        }

        internal VideoOEmbed GetVideoOEmbed(long videoId, int width, int maxWidth, int height, int maxHeight, bool byline, bool title, bool portrait, string color, bool autoPlay, bool apiEnabled, bool xhtml)
        {
            return GetVideoOEmbed(new VideoOEmbedRequestSettings(videoId)
            {
                Width = width,
                MaxWidth = maxWidth,
                Height = height,
                MaxHeight = maxHeight,
                Byline = byline,
                Title = title,
                Portrait = portrait,
                Color = color,
                AutoPlay = autoPlay,
                XHtml = xhtml,
                ApiEnabled = apiEnabled

            });
        }

        public VideoOEmbed GetVideoOEmbed(VideoOEmbedRequestSettings requestSettings)
        {
            return VimeoOEmbedApiClient.GetVideoOEmbedData(requestSettings);
        }

        /// <summary>
        /// Add the VimeoVideo to the cache
        /// </summary>
        /// <param name="video">the vimeoVideo to be cached</param>
        /// <returns>the same vimeoVideo</returns>
        private VimeoVideo CacheVimeoVideo(VimeoVideo video)
        {
            this.MemoryCache.Set(
                new CacheItem(video.VideoId.ToString(), video),
                new CacheItemPolicy() { SlidingExpiration = new System.TimeSpan(0, 5, 0) });

            return video;
        }
    }
}