﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Vimeo
{
    using BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model;
    using RestSharp;

    public class VimeoOEmbedApiClient
    {
        public static VideoOEmbed GetVideoOEmbedData(VideoOEmbedRequestSettings requestSettings)
        {
            var oEmbedClient = new RestClient("https://vimeo.com/api/oembed.json");

            var oEmbedRequest = new RestRequest();
            oEmbedRequest.AddParameter("url", string.Format("https://vimeo.com/{0}", requestSettings.VideoId));
            oEmbedRequest.AddParameter("width", requestSettings.Width);
            oEmbedRequest.AddParameter("maxwidth", requestSettings.MaxWidth);
            oEmbedRequest.AddParameter("height", requestSettings.Height);
            oEmbedRequest.AddParameter("maxheight", requestSettings.MaxHeight);
            oEmbedRequest.AddParameter("byline", requestSettings.Byline);
            oEmbedRequest.AddParameter("title", requestSettings.Title);
            oEmbedRequest.AddParameter("portrait", requestSettings.Portrait);
            oEmbedRequest.AddParameter("color", requestSettings.Color);
            oEmbedRequest.AddParameter("autoplay", requestSettings.AutoPlay);
            oEmbedRequest.AddParameter("xhtml", requestSettings.XHtml);
            oEmbedRequest.AddParameter("api", requestSettings.ApiEnabled);
            oEmbedRequest.AddParameter("player_id", requestSettings.PlayerId);

            var response = oEmbedClient.Execute<VideoOEmbed>(oEmbedRequest);

            return response.Data;
        }
    }
}