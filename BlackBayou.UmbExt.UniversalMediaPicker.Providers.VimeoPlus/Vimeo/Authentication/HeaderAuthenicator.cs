﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Vimeo.Authentication
{
    using RestSharp;
    using RestSharp.Authenticators;

    internal class HeaderAuthenticator : IAuthenticator
    {
        public string HeaderKey { get; set; }
        public string HeaderValue { get; set; }

        public HeaderAuthenticator(string headerKey, string headerValue)
        {
            this.HeaderKey = headerKey;
            this.HeaderValue = headerValue;
        }
        public void Authenticate(IRestClient client, IRestRequest request)
        {
            request.AddHeader(HeaderKey, HeaderValue);
        }
    }
}