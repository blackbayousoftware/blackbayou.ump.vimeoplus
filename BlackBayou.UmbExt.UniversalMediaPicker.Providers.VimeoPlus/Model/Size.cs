﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model
{
    public class Size
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Link { get; set; }
    }
}