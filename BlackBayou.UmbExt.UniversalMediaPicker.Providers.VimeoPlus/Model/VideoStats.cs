﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model
{
    public class VideoStats
    {
        public int Comments { get; set; }
        public int Likes { get; set; }
        public int Plays { get; set; }
    }
}