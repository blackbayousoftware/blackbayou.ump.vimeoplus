﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model
{
    public class ApiResponse<T>
    {
        public int Total { get; set; }
        public int Page { get; set; }
        public int PerPage { get; set; }
        public ApiResponsePageInfo Paging { get; set; }
        public T Data { get; set; }
    }
}