﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model
{
    public class VideoEmbed
    {
        public string Html { get; set; }
    }
}