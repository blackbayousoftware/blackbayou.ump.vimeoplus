﻿//namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus
//{
//    using BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model;
//    using BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Vimeo;
//    using System.Collections.Generic;
//    using System.Linq;

//    public class VimeoPlusService
//    {
//        #region Private Fields
//        private Person _vimeoUser;
//        private VimeoPlusConfig _config;
//        #endregion

//        #region Properties
//        public Person VimeoUser
//        {
//            get
//            {
//                if (_vimeoUser == null)
//                {
//                    var response = api.People.GetCurrentUserInfo();
//                    if (response.Success)
//                    {
//                        _vimeoUser = response.Person;
//                    }
//                }
//                return _vimeoUser;
//            }
//        }

//        private VimeoConnection VimeoConnection
//        {
//            get { return VimeoManager.GetVimeoConnection(_config.AuthKey); }
//        }
//        #endregion

//        #region Constructor
//        public VimeoPlusService(VimeoPlusConfig vimeoConfig)
//        {
//            _config = vimeoConfig;
//        }
//        #endregion

//        #region Public Methods
//        //public IList<Album> GetAlbums()
//        //{
//        //    var albums = new List<Album>();
//        //    if (VimeoUser != null)
//        //    {
//        //        var moreRecords = true;
//        //        var page = 1;
//        //        while (moreRecords)
//        //        {
//        //            var response = api.Albums.GetAlbums(VimeoUser.Username, page: page);

//        //            if (response.Success && response.Albums.OnThisPage > 0)
//        //            {
//        //                albums.AddRange(response.Albums.Items);
//        //            }

//        //            // Keep looping as long as we get successes and there are more total records that the current page * pageSize
//        //            moreRecords = response.Success && (response.Albums.Total > (response.Albums.Perpage & response.Albums.Page));
//        //            page += 1;
//        //        }
//        //    }

//        //    return albums;
//        //}

//        //public IList<Video> GetAlbumVideos(int albumId)
//        //{
//        //    var videos = new List<Video>();
//        //    if (VimeoUser != null)
//        //    {
//        //        var moreRecords = true;
//        //        var page = 1;
//        //        while (moreRecords)
//        //        {
//        //            var response = api.Albums.GetAlbumVideos(albumId, page: page);

//        //            if (response.Success && response.Videos.OnThisPage > 0)
//        //            {
//        //                videos.AddRange(response.Videos.Items);
//        //            }

//        //            // Keep looping as long as we get successes and there are more total records that the current page * pageSize
//        //            moreRecords = response.Success && (response.Videos.Total > (response.Videos.Perpage & response.Videos.Page));
//        //            page += 1;
//        //        }
//        //    }

//        //    return videos;
//        //}

//        //public IList<Channel> GetChannels()
//        //{
//        //    var records = new List<Channel>();
//        //    if (VimeoUser != null)
//        //    {
//        //        var moreRecords = true;
//        //        var page = 1;
//        //        while (moreRecords)
//        //        {
//        //            var response = api.Channels.GetModeratedChannels(VimeoUser.Username, page: page);

//        //            if (response.Success && response.Channels.OnThisPage > 0)
//        //            {
//        //                records.AddRange(response.Channels.Items);
//        //            }

//        //            // Keep looping as long as we get successes and there are more total records that the current page * pageSize
//        //            moreRecords = response.Success && (response.Channels.Total > (response.Channels.Perpage & response.Channels.Page));
//        //            page += 1;
//        //        }
//        //    }

//        //    return records;
//        //}

//        //public IList<Video> GetChannelVideos(int channelId)
//        //{
//        //    var videos = new List<Video>();
//        //    if (VimeoUser != null)
//        //    {
//        //        var moreRecords = true;
//        //        var page = 1;
//        //        while (moreRecords)
//        //        {
//        //            var response = api.Channels.GetChannelVideos(channelId, page: page);

//        //            if (response.Success && response.Videos.OnThisPage > 0)
//        //            {
//        //                videos.AddRange(response.Videos.Items);
//        //            }

//        //            // Keep looping as long as we get successes and there are more total records that the current page * pageSize
//        //            moreRecords = response.Success && (response.Videos.Total > (response.Videos.Perpage & response.Videos.Page));
//        //            page += 1;
//        //        }
//        //    }

//        //    return videos;
//        //}

//        //public IList<Group> GetGroups()
//        //{
//        //    var records = new List<Group>();
//        //    if (VimeoUser != null)
//        //    {
//        //        var moreRecords = true;
//        //        var page = 1;
//        //        while (moreRecords)
//        //        {
//        //            var response = api.Groups.GetGroups(VimeoUser.Username, page: page);

//        //            if (response.Success && response.Groups.OnThisPage > 0)
//        //            {
//        //                records.AddRange(response.Groups.Items);
//        //            }

//        //            // Keep looping as long as we get successes and there are more total records that the current page * pageSize
//        //            moreRecords = response.Success && (response.Groups.Total > (response.Groups.Perpage & response.Groups.Page));
//        //            page += 1;
//        //        }
//        //    }

//        //    return records;
//        //}

//        //public IList<Video> GetGroupVideos(int channelId)
//        //{
//        //    var videos = new List<Video>();
//        //    if (VimeoUser != null)
//        //    {
//        //        var moreRecords = true;
//        //        var page = 1;
//        //        while (moreRecords)
//        //        {
//        //            var response = api.Groups.GetGroupVideos(channelId, page: page);

//        //            if (response.Success && response.Videos.OnThisPage > 0)
//        //            {
//        //                videos.AddRange(response.Videos.Items);
//        //            }

//        //            // Keep looping as long as we get successes and there are more total records that the current page * pageSize
//        //            moreRecords = response.Success && (response.Videos.Total > (response.Videos.Perpage & response.Videos.Page));
//        //            page += 1;
//        //        }
//        //    }

//        //    return videos;
//        //}

//        public IList<VimeoVideo> GetVideos()
//        {
//            return VimeoConnection.GetVideos(new VideoSearchOptions
//            {
//                UserId = _config.UserId
//            });
//        }

//        public VimeoVideo GetVideo(long videoId)
//        {
//            return VimeoConnection.GetVimeoVideo(videoId);
//        }

//        public VideoOEmbed GetVideoEmbedCode(long videoId,
//            int width = 640,
//            int maxWidth = -1,
//            int height = 400,
//            int maxHeight = -1,
//            bool byline = true,
//            bool title = true,
//            bool portrait = true,
//            string color = null,
//            bool autoPlay = false,
//            bool apiEnabled = false,
//            bool xhtml = false)
//        {
//            return VimeoConnection.GetVideoOEmbed(new VideoOEmbedRequestSettings(videoId)
//            {
//                Width = width,
//                MaxWidth = maxWidth,
//                Height = height,
//                MaxHeight = maxHeight,
//                Byline = byline,
//                Title = title,
//                Portrait = portrait,
//                Color = color,
//                AutoPlay = autoPlay,
//                XHtml = xhtml,
//                ApiEnabled = apiEnabled

//            });
//        }
//        #endregion
//    }
//}