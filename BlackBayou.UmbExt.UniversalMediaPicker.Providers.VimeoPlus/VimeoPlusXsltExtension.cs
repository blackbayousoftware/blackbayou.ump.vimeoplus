﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus
{
    using System;
    using System.Linq;
    using System.Xml;
    using System.Xml.XPath;
    using TheOutfield.UmbExt.UniversalMediaPicker.Xslt;
    using TheOutfield.UmbExt.UniversalMediaPicker.Extensions;
    using Umbraco.Core;

    public class VimeoPlusXsltExtension : AbstractXsltExtension
    {
        public static XPathNodeIterator GetMedia(int docTypeDefinitionId, long videoId)
        {
            var service = GetProvider<VimeoPlusProvider>(docTypeDefinitionId).VimeoConnection;

            try
            {
                var video = service.GetVideo(videoId); 

                var xd = new XmlDocument();
                var docNode = XmlHelper.AddTextNode(xd, "Video", "");

                if (video != null)
                {
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "Id", video.VideoId.ToString()));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "Url", video.Uri));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "Title", video.Name));
                    docNode.AppendChild(XmlHelper.AddCDataNode(xd, "Description", video.Description));
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "ThumbnailSmall", video.Thumbnails.Items.Select(t => t.);
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "ThumbnailMedium", video.ThumbnailMedium));
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "ThumbnailLarge", video.ThumbnailLarge));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "Duration", video.Duration.ToString()));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "Width", video.Width.ToString()));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "Height", video.Height.ToString()));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "UploadDate", video.CreatedTime.ToString("yyyy-MM-dd HH:mm:ss")));
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "UserName", video.Owner.Username));
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "UserUrl", video.Owner.ProfileUrl));
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "UserPortraitSmall", video.Owner.Portraits.Items.Select(p => p.Url).FirstOrDefault()));
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "UserPortraitMedium", video.UserPortraitMedium));
                    //docNode.AppendChild(XmlHelper.AddTextNode(xd, "UserPortraitLarge", video.UserPortraitLarge));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "StatsNumberOfLikes", video.Stats.Likes.ToString()));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "StatsNumberOfPlays", video.Stats.Plays.ToString()));
                    docNode.AppendChild(XmlHelper.AddTextNode(xd, "StatsNumberOfComments", video.Stats.Comments.ToString()));
                    docNode.AppendChild(XmlHelper.AddCDataNode(xd, "EmbedCode", service.GetVideoOEmbed(new Model.VideoOEmbedRequestSettings(videoId)).Html));
                }

                xd.AppendChild(docNode);

                return xd.CreateNavigator().Select("/Video");
            }
            catch (Exception e)
            {
                return e.ToXPathNodeIterator();
            }
        }

        public static string GetEmbedCode(int docTypeDefinitionId, 
            long id, 
            int width,
            int maxWidth,
            int height,
            int maxHeight,
            bool byline,
            bool title,
            bool portrait,
            string color,
            bool autoPlay,
            bool apiEnabled,
            bool xhtml)
        {
            var service = GetProvider<VimeoPlusProvider>(docTypeDefinitionId).VimeoConnection;
            return service.GetVideoOEmbed(id, 
                width,
                maxWidth,
                height,
                maxHeight,
                byline,
                title,
                portrait,
                color,
                autoPlay,
                apiEnabled,
                xhtml).Html;
        }
    }
}