﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus
{
    using System;

    public class VimeoPlusConfig
    {
        public string AuthKey { get; set; }

        public string UserId { get; set; }

        public string SearchText { get; set; }

        public string SortBy { get; set; }

        public string SortDirection { get; set; }

        public string LicenseFilter { get; set; }

        public int ThumbnailHeight { get; set; }

        public int PageSize { get; set; }

        public VimeoPlusConfig()
        {
            ThumbnailHeight = 150;
            PageSize = 30;
            SortBy = "alphabetical";
            SortDirection = "asc";
        }

        internal object DeserializeJsonTo<T>()
        {
            throw new NotImplementedException();
        }
    }
}