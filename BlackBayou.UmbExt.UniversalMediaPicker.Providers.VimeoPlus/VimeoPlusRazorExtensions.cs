﻿using System;
using System.Web.Mvc;
using System.Xml.Linq;
using TheOutfield.UmbExt.UniversalMediaPicker; 

namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus
{
    public static class VimeoPlusRazorExtensions
    {
        public static Model.VideoOEmbed VimeoPlusEmbedCode(this HtmlHelper helper, int dataTypeId, long id)
        {
            var service = UniversalMediaPickerProviderFactory.GetProvider<VimeoPlusProvider>(dataTypeId).VimeoConnection;
            return service.GetVideoOEmbed(new Model.VideoOEmbedRequestSettings(id));
        }

        public static Model.VideoOEmbed VimeoPlusEmbedCode(this HtmlHelper helper, XElement element)
        {
            if (element != null && !string.IsNullOrEmpty(element.Value))
            {
                var dataTypeId = Convert.ToInt32(element.Attribute("dataTypeId").Value);
                var videoId = Convert.ToInt64(element.Value);
                return helper.VimeoPlusEmbedCode(dataTypeId, videoId);
            }

            return null;
        }

        public static Model.VideoOEmbed VimeoPlusEmbedCode(this HtmlHelper helper, int docTypeDefinitionId,
           long id,
           int width,
           int maxWidth,
           int height,
           int maxHeight,
           bool byline,
           bool title,
           bool portrait,
           string color,
           bool autoPlay,
           bool apiEnabled,
           bool xhtml)
        {
            var service = UniversalMediaPickerProviderFactory.GetProvider<VimeoPlusProvider>(docTypeDefinitionId).VimeoConnection;
            return service.GetVideoOEmbed(
                id,
                width,
                maxWidth,
                height,
                maxHeight,
                byline,
                title,
                portrait,
                color,
                autoPlay,
                apiEnabled,
                xhtml);
        }
    }
}