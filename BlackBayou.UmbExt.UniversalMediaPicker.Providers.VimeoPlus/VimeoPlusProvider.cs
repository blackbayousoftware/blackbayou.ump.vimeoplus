﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using TheOutfield.UmbExt.UniversalMediaPicker;
    using TheOutfield.UmbExt.UniversalMediaPicker.Extensions;
    using TheOutfield.UmbExt.UniversalMediaPicker.Interfaces;
    using TheOutfield.UmbExt.UniversalMediaPicker.Providers;


    public class VimeoPlusProvider : AbstractProvider
    {
        #region Private Fields
        private VimeoPlusConfigControl _configControl;
        private VimeoPlusConfig _vimeoConfig;

        #endregion

        #region Properties
        internal Vimeo.VimeoConnection VimeoConnection
        {
            get
            {
                return Vimeo.VimeoManager.GetVimeoConnection(_vimeoConfig.AuthKey);
            }

        }

        public override string Alias
        {
            get { return "VimeoPlus"; }
        }
        #endregion

        #region Constructors
        public VimeoPlusProvider()
        {

        }
        public VimeoPlusProvider(string jsonConfig)
            : base(jsonConfig)
        {
            _vimeoConfig = jsonConfig.DeserializeJsonTo<VimeoPlusConfig>();
        }
        #endregion


        public override IConfigControl ConfigControl
        {
            get
            {
                if (_configControl == null)
                    _configControl = new VimeoPlusConfigControl(Config);

                return _configControl;
            }
        }

        public override ICreateControl CreateControl
        {
            get { throw new NotImplementedException(); }
        }

        public override MediaItem GetMediaItemById(string id)
        {
            long videoId;
            if (long.TryParse(id, out videoId))
            {
                return VimeoConnection.GetVideo(videoId).ToMediaItem();
            }
            return null;
        }

        public override IList<MediaItem> GetMediaItems(string parentId)
        {
            var mediaItems = new List<MediaItem>();

            if (parentId == "-1")
            {
                // No need to support albums, groups, or channels as we don't use thems
                //// Create root folders
                //mediaItems.Add(new MediaItem
                //{
                //    Id = "albm:-1",
                //    Title = "Albums",
                //    Icon = UmbracoIcons.FolderOpen,
                //    HasChildren = true,
                //    PreviewImageUrl = "",
                //    Selectable = false
                //});

                //mediaItems.Add(new MediaItem
                //{
                //    Id = "chnl:-1",
                //    Title = "Channels",
                //    Icon = UmbracoIcons.FolderOpen,
                //    HasChildren = true,
                //    PreviewImageUrl = "",
                //    Selectable = false
                //});

                //mediaItems.Add(new MediaItem
                //{
                //    Id = "grp:-1",
                //    Title = "Groups",
                //    Icon = UmbracoIcons.FolderOpen,
                //    HasChildren = true,
                //    PreviewImageUrl = "",
                //    Selectable = false
                //});

                mediaItems.Add(new MediaItem
                {
                    Id = "all:-1",
                    Title = "All",
                    Icon = UmbracoIcons.FolderOpen,
                    HasChildren = true,
                    PreviewImageUrl = "",
                    Selectable = false
                });
            }
            else
            {
                var key = parentId.Substring(0, parentId.IndexOf(":"));
                var id = Convert.ToInt32(parentId.Substring(parentId.IndexOf(":") + 1));

                switch (key)
                {
                    // No need to support albums, groups, or channels as we don't use thems
                    //case "albm":
                    //    if (id == -1)
                    //    {
                    //        // Get Albums
                    //        foreach (var mi in Service.GetAlbums().Select(a => a.ToMediaItem()).OrderBy(mi => mi.Title).ToList())
                    //        {
                    //            mi.Id = "albm:" + mi.Id;
                    //            mediaItems.Add(mi);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        // Get Album Videos
                    //        mediaItems.AddRange(Service.GetAlbumVideos(id).Select(v => v.ToMediaItem()).OrderBy(mi => mi.Title).ToList());
                    //    }
                    //    break;
                    //case "chnl":
                    //    if (id == -1)
                    //    {
                    //        // Get Channels
                    //        foreach (var mi in Service.GetChannels().Select(a => a.ToMediaItem()).OrderBy(mi => mi.Title).ToList())
                    //        {
                    //            mi.Id = "chnl:" + mi.Id;
                    //            mediaItems.Add(mi);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        // Get Channel Videos
                    //        mediaItems.AddRange(Service.GetChannelVideos(id).Select(v => v.ToMediaItem()).OrderBy(mi => mi.Title).ToList());
                    //    }
                    //    break;
                    //case "grp":
                    //    if (id == -1)
                    //    {
                    //        // Get Groups
                    //        foreach (var mi in Service.GetGroups().Select(a => a.ToMediaItem()).OrderBy(mi => mi.Title).ToList())
                    //        {
                    //            mi.Id = "grp:" + mi.Id;
                    //            mediaItems.Add(mi);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        // Get Group Videos
                    //        mediaItems.AddRange(Service.GetGroupVideos(id).Select(v => v.ToMediaItem()).OrderBy(mi => mi.Title).ToList());
                    //    }
                    //    break;
                    default:
                        // Get All Videos
                        mediaItems.AddRange(
                            VimeoConnection.GetVideos(new Model.VideoSearchOptions
                            {
                                UserId = _vimeoConfig.UserId
                            })
                            .Select(v => v.ToMediaItem())
                            .OrderBy(mi => mi.Title)
                            .ToList()
                        );
                        break;
                }
            }

            return mediaItems;
        }


    }
}