﻿namespace BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus
{
    using System;
    using System.Linq;
    using TheOutfield.UmbExt.UniversalMediaPicker;
    using BlackBayou.UmbExt.UniversalMediaPicker.Providers.VimeoPlus.Model;

    public static class VimeoPlusExtensions
    {
        #region UMP

        //public static MediaItem ToMediaItem(this Group group)
        //{
        //    return new MediaItem
        //    {
        //        Id = group.Id.ToString(),
        //        Title = group.Name,
        //        Icon = UmbracoIcons.FolderOpen,
        //        HasChildren = group.TotalVideos > 0,
        //        PreviewImageUrl = "",
        //        Selectable = false
        //    };
        //}

        //public static MediaItem ToMediaItem(this Channel channel)
        //{
        //    return new MediaItem
        //    {
        //        Id = channel.Id.ToString(),
        //        Title = channel.Name,
        //        Icon = UmbracoIcons.FolderOpen,
        //        HasChildren = channel.TotalVideos > 0,
        //        PreviewImageUrl = "",
        //        Selectable = false
        //    };
        //}

        //public static MediaItem ToMediaItem(this Album album)
        //{
        //    return new MediaItem
        //    {
        //        Id = album.Id.ToString(),
        //        Title = album.Title,
        //        Icon = UmbracoIcons.FolderOpen,
        //        HasChildren = album.TotalVideos > 0,
        //        PreviewImageUrl = "",
        //        Selectable = false
        //    };
        //}

        public static MediaItem ToMediaItem(this VimeoVideo video)
        {
            var mi = new MediaItem
            {
                Id = video.VideoId.ToString(),
                Title = video.Name,
                Icon = "vimeoMedia.png",
                HasChildren = false,
                PreviewImageUrl = video.Pictures.Where(p=> p.Type == "custom").Select(picture => picture.Sizes.Select(ps => ps.Link).FirstOrDefault()).FirstOrDefault(),
                Selectable = true
            };

            var t = TimeSpan.FromSeconds(video.Duration);
            var duration = string.Format("{0:D2}:{1:D2}",
                                    t.Minutes,
                                    t.Seconds);

            mi.MetaData.Add("Description", video.Description);
            mi.MetaData.Add("VideoID", video.VideoId.ToString());
            mi.MetaData.Add("Url", video.Link); 
            mi.MetaData.Add("Duration", duration);
            mi.MetaData.Add("Width", video.Width.ToString());
            mi.MetaData.Add("Height", video.Height.ToString());
            mi.MetaData.Add("Upload Date", video.CreatedTime.ToShortDateString());


            return mi;
        }

        #endregion
    }
}