﻿#Universal Media Picker - Vimeo Plus Provider

##Description
Installs the Vimeo Plus Provider for the Universal Media Picker.

Once installed, create a new data type, and choose "UniversalMediaPicker" from the Render Control field, and select Vimeo from the provider dropdown to setup your Vimeo media picker.

NB: You MUST install the Universal Media Picker package at [http://our.umbraco.org/projects/backoffice-extensions/universal-media-picker before installing this package](http://our.umbraco.org/projects/backoffice-extension).

##Breaking Change
v 2.0.0 - This version replaced the dependency on BlackBayou.Vimeo since that library used the now deprecated Advanced API. 
This version now uses the latest RESTful API by Vimeo. Several model classes have been changed and are now located in 
the Models namespace.

##Source
Source for this project can be found at [https://bitbucket.org/blackbayousoftware/blackbayou.ump.vimeoplus](https://bitbucket.org/blackbayousoftware/blackbayou.ump.vimeoplus).

##License
This project is licensed under the [MIT License](http://opensource.org/licenses/MIT).

## Dependencies
This project depends on [BlackBayou.Vimeo](https://bitbucket.org/blackbayousoftware/blackbayou.vimeo) for access to Vimeo's Advanced API methods.

## Thanks
This project leans heavily on the [Vimeo Provider](http://umpp4umb.codeplex.com/) for Universal Media Picker. 
Thank you Matt Brailsford for the hard work and awesome plugin.